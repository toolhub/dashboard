import './App.css';
import { AppBar, Box, Toolbar, IconButton, Button, Typography, Container } from '@mui/material';
import { BrowserRouter, Route } from 'react-router-dom';
import MenuIcon from '@mui/icons-material/Menu';

import './services/toolstudio';

import { Home } from './pages/home/Home';
import { FunctionPlayer } from './pages/player/Player';

function App() {

	return (
		<Box sx={{ flexGrow: 1 }}>
			<AppBar position="sticky">
				<Container maxWidth="md">
					<Toolbar disableGutters={true}>
						<IconButton size="large" edge="start" color="inherit" aria-label="menu" sx={{ mr: 2 }}>
							<MenuIcon />
						</IconButton>
						<Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
							ToolStudio
						</Typography>
						<Button color="secondary" size="small" variant="contained" sx={{ mx: "2px" }}>log in</Button>
						<Button color="secondary" size="small" variant="contained" sx={{ mx: "2px" }}>join</Button>
					</Toolbar>
				</Container>
			</AppBar>
			<BrowserRouter>
				<Route exact path="/" component={Home} />
				<Route path="/funcs/:group/:name" render={ props => (
					<FunctionPlayer name={props.match.params.name} group={props.match.params.group}/>
					)} />
			</BrowserRouter>
		</Box>
	);
}

export default App;
